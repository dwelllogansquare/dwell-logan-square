Logan Square, located only 10 minutes away from Auburn University boasts comfort and convenient off-campus student living with best in class finishes and amenities that are tailored to suit your lifestyle.

Address: 733 W Glenn Avenue, Auburn, AL 36832, USA

Phone: 334-826-6470

Website: https://www.dwellstudentauburn.com/